# Programming task - Badger Maps

`data_analysis_badger.py` contains the code doing the required tasks.  
The data is represented by an object "Data_Analysis" whose methods will give the answers. They use the `pandas` library to tackle them. Only the names of the csv file has to be changed to print the answers when executing.

There is no exception handling as missing values are handled naturally by pandas.
