import numpy as np
import pandas as pd

class Data_Analysis():
    """
    Brief   : Class for databases of customers
    """
    def __init__(self, csv_file):
        self.csv_file = csv_file # Name of the csvfile
        self.db = pd.DataFrame(columns = [
            "First Name", 
            "Last Name", 
            "Street", 
            "Zip", 
            "City", 
            "Type", 
            "Last Check-In Date", 
            "Job",
            "Phone",
            "Company"
            ]) #DataBase
    
    def read_data(self):
        """
        Brief   : Updates the database of the object
        """
        self.db = pd.read_csv(self.csv_file)
        self.db["Last Check-In Date"] = pd.to_datetime(self.db["Last Check-In Date"], format="%d/%m/%Y")

    def index_to_fullname(self,i):
        """
        Brief   : Returns the full name of the customer given their index in the dataframe
        """
        return self.db.loc[i, "First Name"] + " " + self.db.loc[i, "Last Name"]

    def get_earliest_checkdate(self):
        """
        Brief   : Returns the full name of the customer with the earliest check-in date
        """
        return self.index_to_fullname(self.db["Last Check-In Date"].idxmin())

    def get_latest_checkdate(self):
        """
        Brief   : Returns the full name of the customer with the latest check-in date
        """
        return self.index_to_fullname(self.db["Last Check-In Date"].idxmax())

    def get_fullnames(self):
        """
        Brief   : Returns the list of all the customers' full names ordered alphabetically
        """
        df_names = self.db[["First Name", "Last Name"]].dropna()
        df_fnames = df_names["First Name"] +  " " + df_names["Last Name"]
        return list(df_fnames.sort_values())
    
    def get_jobs(self):
        """
        Brief   : Returns the list of all the customers' jobs ordered alphabetically and without repetition
        """
        df_jobs = pd.unique(self.db["Job"].dropna().sort_values())
        return list(df_jobs)
    
    def print_all_analysis(self):
        self.read_data()
        print("\nCustomer with the earliest check in date:")
        print(self.get_earliest_checkdate())
        print("\nCustomer with the latest check in date:")
        print(self.get_latest_checkdate())
        print("\nList of customers' full names:")
        print(self.get_fullnames())
        print("\nList of customers' jobs:")
        print(self.get_jobs())

if __name__ == "__main__":
    customers_data = Data_Analysis("Sample test file - Sheet1.csv")
    customers_data.print_all_analysis()
    
